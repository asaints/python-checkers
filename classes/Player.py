class Player:
	def __init__(self, name, side):
		self.name = name
		self.side = side
		
		self.playerpieces = []

	def playerTurn(self, board):
		#prompt select piece to move
		input_selectedTile = "e3"
		selectedTile = board.boardGetTile(input_selectedTile)

		input_destinationTile = "d4"
		destinationTile = board.boardGetTile(input_destinationTile)
	
		if self.validateMove(board, selectedTile, destinationTile) == True:
			self.moveChip(selectedTile.occupant, selectedTile, destinationTile)

		

		

	def validateMove(self, board, srctile, desttile):
		print('Validating move')
		allowMove = False
		sourcechip = srctile.occupant

		enemytiles = self.scanChipArea(board, srctile)

		#display enemytiles TESTING
		for tile in enemytiles:
			print(tile.occupant.side + " " + tile.tilename + " " + str(tile.tilecoordinates))


		if len(enemytiles) > 0:
			# there are enemy chips
			pass

		else:
			#there are no enemy chips, normal move
			if sourcechip.dama == False:
				#non-dama
				isDiagonal, stepCountCorrect, isRightDirection = self.runMoveChecks(1, board, srctile, desttile)

				if isDiagonal == True and stepCountCorrect == True and isRightDirection == True:
					allowMove = True

			else:
				#for dama
				pass

		return allowMove


	def runMoveChecks(self, tilestep, board, srctile, desttile):
		isDiagonal = False
		stepCountCorrect = False
		isRightDirection = False

		if tilestep == 1:
			#for normal case
			#1. check if diagonal
			def diagonalCheck():
				if abs(board.NAME_MAPPING.index(srctile.tilename[0]) - board.NAME_MAPPING.index(desttile.tilename[0])) == 1:
					return True

				else:
					return False

			isDiagonal = diagonalCheck()
			print('isDiagonal = ' + str(isDiagonal))

			#2. check if 1 step distance
			def stepCountCheck():
				if abs(int(srctile.tilename[1]) - int(desttile.tilename[1])) == 1:
					return True

				else:
					return False

			stepCountCorrect = stepCountCheck()
			print('stepCountCorrect = ' + str(stepCountCorrect))

			#3. check if 1 step is correct direction
			def directionCheck():

				if int(srctile.tilename[1]) - int(desttile.tilename[1]) < 0:
					return (True if srctile.occupant.side == 'white' else False)

				else:
					return (True if srctile.occupant.side == 'black' else False)

			isRightDirection = directionCheck()
			print('isRightDirection = ' + str(isRightDirection))

		else:
			#for dama case
			pass

		return tuple([isDiagonal, stepCountCorrect, isRightDirection])



	def scanChipArea(self, board, srctile):
		checkerboard = board.board
		enemytiles = []

		if srctile.occupant != None and srctile.occupant.dama == False:
			topleft = [1, -1]
			topright = [1, 1]
			bottomleft = [-1, -1]
			bottomright = [-1, 1]
			
			directions = [topleft, topright, bottomleft, bottomright]
			srcCoordinates = srctile.tilecoordinates

			

			for direction in directions:
				basetile = checkerboard[srcCoordinates[0]][srcCoordinates[1]]

				try:
					if (srcCoordinates[0] + direction[0] > 0) and (srcCoordinates[1] + direction[1] > 0):#added this restriction because negative list index wraps around
						scantile = checkerboard[srcCoordinates[0] + direction[0]][srcCoordinates[1] + direction[1]]

						if scantile.occupant != None and scantile.occupant.side == ('black' if basetile.occupant.side == 'white' else 'white'):
							print(str(srcCoordinates[0]) + " + " + str(direction[0]) + "     " + str(srcCoordinates[1]) + " + " + str(direction[1]))
							enemytiles.append(scantile)


				except IndexError:
					pass

		else:
			#code for dama case
			pass

		return enemytiles
		

	def moveChip(self, chip, srctile, desttile):
		srctile.occupant = None
		desttile.occupant = chip
