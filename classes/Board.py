from Tile import Tile
from Chip import Chip

class Board:
	NAME_MAPPING = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h']

	def __init__(self, players):
		self.player1, self.player2 = players

		sampletile = Tile('e1', [0, 0])

		self.board = []

		def createBoard():
			for row in range(8):
				row = [Tile(self.NAME_MAPPING[x] + str(row + 1), [row, x]) for x in range(8)]

				self.board.append(row)

		createBoard()

		def initializePieces():
			whitechiptiles = []
			
			whitechiptiles += [x for index, x in enumerate(self.board[0]) if index % 2 == 0]
			whitechiptiles += [x for index, x in enumerate(self.board[1]) if index % 2 == 1]
			whitechiptiles += [x for index, x in enumerate(self.board[2]) if index % 2 == 0]
			
			for tile in whitechiptiles:
				tile.occupant = Chip('white')
				self.player1.playerpieces.append(tile.occupant)

			blackchiptiles = []
			
			blackchiptiles += [x for index, x in enumerate(self.board[5]) if index % 2 == 1]
			blackchiptiles += [x for index, x in enumerate(self.board[6]) if index % 2 == 0]
			blackchiptiles += [x for index, x in enumerate(self.board[7]) if index % 2 == 1]
			
			for tile in blackchiptiles:
				tile.occupant = Chip('black')
				self.player2.playerpieces.append(tile.occupant)

		initializePieces()

		self.show()

		self.playGame()


	def show(self):	
		'''
		a8[7, 0]   b8[7, 1]   c8[7, 2]   d8[7, 3]   e8[7, 4]   f8[7, 5]   g8[7, 6]   h8[7, 7]

		a7[6, 0]   b7[6, 1]   c7[6, 2]   d7[6, 3]   e7[6, 4]   f7[6, 5]   g7[6, 6]   h7[6, 7]

		a6[5, 0]   b6[5, 1]   c6[5, 2]   d6[5, 3]   e6[5, 4]   f6[5, 5]   g6[5, 6]   h6[5, 7]

		a5[4, 0]   b5[4, 1]   c5[4, 2]   d5[4, 3]   e5[4, 4]   f5[4, 5]   g5[4, 6]   h5[4, 7]

		a4[3, 0]   b4[3, 1]   c4[3, 2]   d4[3, 3]   e4[3, 4]   f4[3, 5]   g4[3, 6]   h4[3, 7]

		a3[2, 0]   b3[2, 1]   c3[2, 2]   d3[2, 3]   e3[2, 4]   f3[2, 5]   g3[2, 6]   h3[2, 7]

		a2[1, 0]   b2[1, 1]   c2[1, 2]   d2[1, 3]   e2[1, 4]   f2[1, 5]   g2[1, 6]   h2[1, 7]

		a1[0, 0]   b1[0, 1]   c1[0, 2]   d1[0, 3]   e1[0, 4]   f1[0, 5]   g1[0, 6]   h1[0, 7]
		'''
		
		print()
		for row in range(7, -1, -1):
			rowstring = str(row + 1) + "   "

			for column in range(8):
				#rowstring += self.board[row][column].tilename + "   " #for tile names
				#rowstring += str(self.board[row][column].tilecoordinates) + "   " #for tile coordinates
				#rowstring += self.board[row][column].tilename + str(self.board[row][column].tilecoordinates) + "   " #for tile name with coordinates
				rowstring += str(self.board[row][column].occupantToString()) + "   " #for tile occupant

			print(rowstring)
			print()

		print("    A" + "   " + "B" + "   " + "C" + "   " + "D" + "   " + "E" + "   " + "F" + "   " + "G" + "   " + "H")
		print()
	
	def playGame(self):
		self.player1.playerTurn(self)

		self.show()

	def boardGetTile(self, tilename):
		return self.board[int(tilename[1]) - 1][self.NAME_MAPPING.index(tilename[0].lower())]