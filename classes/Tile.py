class Tile:
	def __init__(self, tilename, tilecoordinates):
		self.tilename = tilename
		self.tilecoordinates = tilecoordinates
		self.occupant = None

	def occupantToString(self):
		return ('.' if self.occupant == None else ('W' if self.occupant.side == 'white' else 'B'))