Practice Python implementation of a CLI checkers game. 
 
Currently Implemented:
 - Board, Tile, Player, Chip classes
 - Checker board initialization
 - Move chip function
 - Some Move chip validation functions
 
A work-in-progress.
 
Python version: 3.7